import React, { Component } from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { withStyles } from '@material-ui/core'

const styles = {
  '@global': {
    body: {
      backgroundColor: '#fff'
    },
  },
  paper: {
    marginTop: 64,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: 8,
    backgroundColor: '#ff1744',
  },
  form: {
    //width: '100%', // Fix IE 11 issue.
    marginTop: 8,
  },
  submit: {
    margin: '24 0 16',
  },
};

class Register extends Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      passwordconfirm: '',
      isLogin: false,
      users: []
    }
  }
  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    console.log(name, value);
    let data = {}
    data[name] = value;
    this.setState(data);
    //console.log(data);
  }
  handleSubmit = (event) => {
    event.preventDefault();
    console.log("ok")
    const { email, password, passwordconfirm } = this.state;
    if (password === passwordconfirm) {
      console.log("ok")
      axios({
        method: 'POST',
        url: 'http://localhost:3000/users',
        data: {
          email: email,
          password: password
        }
      })
        .then(response => {
          console.log(response.data)
          const token = new Date().getTime();
         // console.log(token)
          localStorage.setItem('token',token);
          this.props.history.push('/');
        })
        .catch(error => {
          console.log(error);
        })
    }
    else console.log('fail');
  }
  render() {
    const { classes } = this.props
    return (
      <>
        <Container component="main" maxWidth="xs" />
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>

          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
        </Typography>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              onChange={this.handleChange}
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              onChange={this.handleChange}
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="passwordconfirm"
              label="Password Confirm"
              onChange={this.handleChange}
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
          </Button>
          </form>
          {/* <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid> */}
        </div>
      </>
    )
  }
}
export default withStyles(styles)(Register)