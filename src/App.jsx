import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Rooms from './pages/Rooms';
import SingleRoom from './pages/SingleRoom';
import Error from './pages/Error';
import About from './pages/About';

import Navbar from './components/Navbar';
import FeatureRooms from './components/FeatureRooms';
import Login from './pages/Login';
import Register from './pages/Register';


function App() {
  return (
    <>
    <Navbar/>
    <Switch>
      <Route exact path = "/" component={Home}/>
      <Route exact path = "/rooms" component={Rooms}/>
      <Route exact path = "/single-room/:slug " component={SingleRoom} /> 
      <Route exact path = "/about-us" component={About}/>
      <Route exact path = "/feature" component={FeatureRooms}/>
      <Route exact path = "/login" component={Login}/>
      <Route exact path = "/register" component={Register}/>
      <Route component={Error}/>
    </Switch>
    </>
  );
}

export default App;
