import React, { Component } from 'react'
import axios from 'axios';


export default class FeatureRooms extends Component {
   constructor(props){
       super(props)
    if(!localStorage.getItem("token")){
        this.props.history.push('/Login')
      }
    this.state = {
        rooms:[],
    }
   }
    
  componentDidMount() {

      axios({
          method: 'GET',
          url: 'http://localhost:3000/rooms',
          data: null
      })
      .then(res=> {
          this.setState({
              rooms: res.data
          })
      })
      .catch(error => {
          console.log(error)
      })
      //GET API JSON SERVER 

  }
    render() {              
        const {rooms} = this.state
        console.log(rooms);
        return (
            <div>
               
               {
                   rooms.map(item =>
                         <h5>{item.title}</h5>
                    )
               }
            </div>
        )
    }
}
